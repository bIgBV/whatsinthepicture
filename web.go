package main

import (
	"encoding/json"
	"github.com/jmoiron/sqlx"
	"github.com/kr/beanstalk"
	"log"
	"net/http"
	"strconv"
)

// Job holds the info for a single job
type Job struct {
	ID           int64  `json:"id"`
	Image        string `json:"image"` // URL of the original image
	Status       string `json:"status"`
	UpdatedImage string `json:"updated_image"db:"updated_image"` // location where the marked image is stored
	Label        string `json:"label"db:"label"`                 // Location of the image with the label text
}

// Context holds app context. Here it is DB connection
// and soon the MQ context
type Context struct {
	db    *sqlx.DB
	queue *beanstalk.Conn
}

func (ctx *Context) jobHandler(w http.ResponseWriter, r *http.Request) {
	jobID := r.URL.Query().Get("id")
	if jobID == "" {
		w.WriteHeader(http.StatusNotAcceptable)
		json.NewEncoder(w).Encode([]byte("Incorrect data"))
		log.Println("Trying to find the details of job: ", jobID)
	}

	id, err := strconv.ParseInt(jobID, 10, 64)
	if err != nil {
		panic(err)
	}
	job, err := GetJob(id, ctx)
	if err != nil {
		log.Println(err)
	}

	w.Header().Set("Content-type", "application/json")
	json.NewEncoder(w).Encode(job)
}

func (ctx *Context) registerJob(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	newJob := &Job{}
	err := decoder.Decode(newJob)

	if err != nil {
		log.Println(err)
		panic(err)
	}

	// We're just printing this for now. Later it'll send
	// a job to the worker
	log.Println("New job request for URL: ", newJob.Image)

	newJob, err = CreateJob(newJob, ctx)
	if err != nil {
		log.Fatalln(err)
	}

	w.Header().Set("Content-type", "application/json")
	json.NewEncoder(w).Encode(newJob)
}

func (ctx *Context) updatejob(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	updatedJob := &Job{}
	err := decoder.Decode(updatedJob)

	if err != nil {
		log.Println(err)
		panic(err)
	}

	// We're just printing this for now. Later it'll send
	// a job to the worker
	log.Println("Recieved update for job: ", updatedJob.ID)

	var newJob *Job
	newJob, err = UpdateJob(updatedJob, ctx)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
		log.Println(err)
	}

	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(newJob)
}

// Log function wraps a handler and logs incoming requests
func Log(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

func main() {
	fs := http.FileServer(http.Dir("static"))

	db, err := InitDB()
	if err != nil {
		panic(err)
	}

	c, err := InitQueue()
	if err != nil {
		panic(err)
	}

	ctx := &Context{db: db, queue: c}

	http.Handle("/", fs)
	http.HandleFunc("/job_status/", ctx.jobHandler)
	http.HandleFunc("/job_register/", ctx.registerJob)
	http.HandleFunc("/job_update", ctx.updatejob)

	log.Println("Listening...")
	http.ListenAndServe(":3000", Log(http.DefaultServeMux))
}
