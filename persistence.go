package main

import (
	"encoding/binary"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/kr/beanstalk"
	_ "github.com/lib/pq" // DB driver
	"log"
	"time"
)

// DB config
const (
	DB_USER     = "tdb"
	DB_PASSWORD = "donotforgetthis"
	DB_NAME     = "traveldb"
)

// InitDB initializes a DB connection
func InitDB() (*sqlx.DB, error) {
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		DB_USER, DB_PASSWORD, DB_NAME)
	db, err := sqlx.Open("postgres", dbinfo)
	if err != nil {
		log.Println("Error in initializing the database")
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	log.Println("Initialized DB connection")

	return db, nil
}

// InitQueue initializes the message queue
func InitQueue() (*beanstalk.Conn, error) {
	c, err := beanstalk.Dial("tcp", "127.0.0.1:11300")
	if err != nil {
		log.Println("Error in initializing the queue")
		return nil, err
	}

	log.Println("Initialized queue connection")
	return c, err
}

// CreateJob creates a new Job in the DB
func CreateJob(job *Job, ctx *Context) (*Job, error) {
	var lastInsertID int64

	// Inserting into DB
	err := ctx.db.QueryRow("INSERT INTO job(status, image) VALUES($1, $2) RETURNING id;", "created", job.Image).Scan(&lastInsertID)
	if err != nil {
		return job, err
	}
	job.ID = lastInsertID
	job.Status = "created" // Horrible. Have to move this to an ENUM

	tempBuf := make([]byte, 8)
	binary.BigEndian.PutUint64(tempBuf, uint64(lastInsertID))

	// Creating Job on the queue
	_, err = ctx.queue.Put([]byte(tempBuf), 1, 0, 120*time.Second)
	return job, err
}

// UpdateJob updates the status of a job in the DB
func UpdateJob(job *Job, ctx *Context) (*Job, error) {
	_, err := ctx.db.Query(
		"UPDATE job SET status=$1, updated_image=$2, label=$3 WHERE id=$4",
		job.Status,
		job.UpdatedImage,
		job.Label,
		job.ID)
	if err != nil {
		return job, err
	}

	return job, err
}

// GetJob returns a specific job with the given ID
func GetJob(jobID int64, ctx *Context) (*Job, error) {

	var job Job
	err := ctx.db.QueryRowx("SELECT id, status, image, updated_image, label FROM job WHERE id=$1", jobID).StructScan(&job)
	if err != nil {
		return nil, err
	}

	return &job, err
}
