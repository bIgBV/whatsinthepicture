CREATE TABLE job
(
    id BIGSERIAL PRIMARY KEY,
    status TEXT,
    image TEXT,
    updated_image TEXT NOT NULL DEFAULT '',
    label TEXT NOT NULL DEFAULT '',
    created TIMESTAMP DEFAULT current_timestamp
);