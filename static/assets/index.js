function initApp() {
    let input = $("#urlInput")
    let presentationContainer = $("#presentationContainer");
    let resultsContainer = $("#resultText")

    function submitJob(imgUrl, successCb, heading) {
        let postData = {
            id: 0,
            image: imgUrl
        };

        $.post('/job_register/', JSON.stringify(postData))
            .done(data=> successCb(data, heading))
            .fail(err=> onError(err, heading));
    }

    function pollJobStatus(job) {
        setTimeout(function(){
            $.ajax({ 
                url: `/job_status/?id=${job.id}`,
                success: (data) => {
                    if (data.status === 'created') {
                        //Setup the next poll recursively
                        pollJobStatus(data);
                    }
                    if (data.status === 'error') {
                        handleError(data);
                        return;
                    }
                    if (data.status === 'completed') {
                        // Update result
                        updateResults(data);
                        return;
                    }
                },
                error: (error) => {
                    console.log(error);
                    onError(err);
                },
                dataType: "json"
            });
        }, 5000);
    }

    function updateResults(resultData) {
        presentationContainer.html("");
        let imageLocation = resultData.updated_image;
        let lableLocation = resultData.label;
        
        let resultsElement = $(`
            <h3>Your image has the following entities:</h3>
            <img src=${imageLocation} class="u-max-full-width" />
            <h3>Labels:</h3>
            <img src=${lableLocation} class="u-max-full-width" />
        `);

        resultsContainer.html(resultsElement);
    }

    function handleError(resultData) {
        presentationContainer.html("");
        let resultsElement = $(`
            <h3>Uh oh, something went wrong :(</h3>
        `);

        resultsContainer.html(resultsElement);
    }

    function onError(err, heading) {
        heading.html("");
        heading.html(`<h3 id="heading">Oh oh something went wrong :( </h3>`);
    }

    function onSubmit(data, heading) {
        heading.html("");
        heading.html(`<h3 id="heading">Your request has been submitted :D </h3>`);
        // Begin polling for data
        pollJobStatus(data);
    }

    function handleFormSubmit(event) {
        event.preventDefault();
        let url = input.val();
        // Load the image to display
        let newElement = $(`<h3 id="heading">You gave the following image: </h3> <img src=${url} class="u-max-full-width" />`);
        presentationContainer.html(newElement);

        let heading = $("h3#heading");
        submitJob(url, onSubmit, heading);
    }

    // Bind event handlers
    $("#urlSubmit").on("click", handleFormSubmit);

    // Exposing the API for onload scripts to call
    window.initApp = initApp;
}